#include "slackbot.h"

#include <string.h>

#include "esp_log.h"
#include "esp_event.h"
#include "esp_http_client.h"
#include "esp_http_server.h"

#include "mbedtls/sha256.h"
#include "cJSON.h"

static const char *TAG = "SLACKBOT";

typedef struct
{
    const char *cmd;
} SLACKBOT_SlachCmd_t;

static SLACKBOT_SlachCmd_t slashCommands[CONFIG_SLACK_MAX_SLASH_COMMANDS];

static inline int ishex(int x)
{
    return (x >= '0' && x <= '9') ||
           (x >= 'a' && x <= 'f') ||
           (x >= 'A' && x <= 'F');
}

static int url_decode(const char *s, char *dec, int size)
{
    int len = (size > 0) ? size : strlen(s);

    char *o;
    const char *end = s + len;
    int c;

    for (o = dec; s <= end; o++)
    {
        c = *s++;
        if (c == '+')
            c = ' ';
        else if (c == '%' && (!ishex(*s++) ||
                              !ishex(*s++) ||
                              !sscanf(s - 2, "%2x", &c)))
            return -1;

        if (dec)
            *o = c;
    }

    return o - dec;
}

static esp_err_t root_post_handler(httpd_req_t *req)
{
    //First check headers for signature verification

    char tmstbuf[32];
    memset(tmstbuf, 0, sizeof(tmstbuf));
    if (httpd_req_get_hdr_value_str(req, "x-slack-request-timestamp", tmstbuf, sizeof(tmstbuf)) != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to decode X-Slack-Request-Timestamp header");
        return ESP_FAIL;
    }

    char reqSha256[4 + 64 + 1];
    memset(reqSha256, 0, sizeof(reqSha256));
    if (httpd_req_get_hdr_value_str(req, "x-slack-signature", reqSha256, sizeof(reqSha256)) != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to decode X-Slack-Signature header");
        return ESP_FAIL;
    }

    char reqVersion[8];
    memset(reqVersion, 0, sizeof(reqVersion));
    for (uint8_t i = 0; i < sizeof(reqVersion); i++)
    {
        if (reqSha256[i] == '=')
        {
            memcpy(reqVersion, reqSha256, i);
            reqVersion[i] = '\0';
            break;
        }
    }
    if (strlen(reqVersion) == 0)
    {
        ESP_LOGE(TAG, "Failed to retrieve version from signature");
        return ESP_FAIL;
    }

    ESP_LOGI(TAG, "Timestamp : %s", tmstbuf);
    ESP_LOGI(TAG, "Signature : %s", reqSha256);
    ESP_LOGI(TAG, "Version : %s", reqVersion);

    char *encodedContent = malloc(req->content_len);

    if (!encodedContent)
    {
        ESP_LOGE(TAG, "Failed to allocate memory for content download");
        return ESP_FAIL;
    }

    if (httpd_req_recv(req, encodedContent, req->content_len) <= 0)
    {
        free(encodedContent);
        return ESP_FAIL;
    }

    int decodedLen = url_decode(encodedContent, NULL, req->content_len);

    if (decodedLen > 0)
    {
        //sig_basestring = 'v0:' + timestamp + ':' + request_body
        size_t contentStartIndex = strlen(reqVersion) + 1 + strlen(tmstbuf) + 1;
        size_t baseLen = contentStartIndex + decodedLen + 1;

        char *content = malloc(baseLen);
        memset(content, 0, baseLen);

        if (content == NULL)
        {
            free(encodedContent);
            ESP_LOGE(TAG, "Failed to allocate response decode buffer");
            return ESP_FAIL;
        }

        snprintf(content, baseLen, "%s:%s:", reqVersion, tmstbuf);
        ESP_LOGI(TAG, "Base string header : %s", content);

        if (url_decode(encodedContent, &content[contentStartIndex], req->content_len) == -1)
        {
            free(encodedContent);
            ESP_LOGE(TAG, "Failed to decode response content");
            return ESP_FAIL;
        }

        ESP_LOGI(TAG, "Full Base : %.*s", baseLen, content);

        uint8_t sha[32];
        mbedtls_sha256((const uint8_t *)content, sizeof(content), sha, 0);

        ESP_LOGI(TAG, "Sha : %.*s", baseLen, content);
        ESP_LOG_BUFFER_HEXDUMP(TAG, sha, sizeof(sha), ESP_LOG_INFO);

        // ESP_LOGI(TAG, "Slash command : %.*s", decodedLen + 1, content);

        char cmd[64] = {'\0'};
        esp_err_t ret = httpd_query_key_value(
            content, "command", cmd, sizeof(cmd) - 1);
        if (ret == ESP_OK)
        {
            ESP_LOGI(TAG, "Command : %s", (const char *)cmd);
        }
    }

    free(encodedContent);

    // End response
    httpd_resp_send(req, "OK", 2);
    return ESP_OK;
}

static httpd_uri_t root = {
    .uri = "/",
    .method = HTTP_POST,
    .handler = root_post_handler,
    .user_ctx = NULL};

static httpd_handle_t start_webserver(void)
{
    httpd_handle_t server = NULL;
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();

    // Start the httpd server
    ESP_LOGI(TAG, "Initializing server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK)
    {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        httpd_register_uri_handler(server, &root);
        return server;
    }

    ESP_LOGI(TAG, "Error starting server!");
    return NULL;
}

static void stop_webserver(httpd_handle_t server)
{
    // Stop the httpd server
    httpd_stop(server);
}

static void disconnect_handler(void *arg, esp_event_base_t event_base,
                               int32_t event_id, void *event_data)
{
    httpd_handle_t *server = (httpd_handle_t *)arg;
    if (*server)
    {
        ESP_LOGI(TAG, "Stopping webserver");
        stop_webserver(*server);
        *server = NULL;
    }
}

static void connect_handler(void *arg, esp_event_base_t event_base,
                            int32_t event_id, void *event_data)
{
    httpd_handle_t *server = (httpd_handle_t *)arg;
    if (*server == NULL)
    {
        ESP_LOGI(TAG, "Starting webserver");
        *server = start_webserver();
    }
}

esp_err_t SLACKBOT_Init(const char *root_path)
{
    if (root_path)
    {
        root.uri = root_path;
    }

    static httpd_handle_t server;

    if (esp_event_loop_create_default() == ESP_ERR_INVALID_STATE)
    {
        ESP_LOGD(TAG, "Default event loop already exists");
    }

    ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &connect_handler, &server));
    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_EVENT, WIFI_EVENT_STA_DISCONNECTED, &disconnect_handler, &server));

    server = start_webserver();

    ESP_LOGI(TAG, "SlackBot Initialized");

    return ESP_OK;
}

esp_err_t SLACKBOT_Post(const char *text)
{
    cJSON *json = cJSON_CreateObject();
    if (strlen(text) > 0)
    {
        if (cJSON_AddStringToObject(json, "text", text) == NULL)
        {
            ESP_LOGE(TAG, "Failed to add element to rxpk json");
        }
    }

    char *jsonStr = cJSON_Print(json);
    cJSON_Delete(json);

    esp_http_client_config_t config = {
        .url = CONFIG_SLACK_WEBHOOK_URL,
        .method = HTTP_METHOD_POST};
    esp_http_client_handle_t client = esp_http_client_init(&config);

    esp_http_client_set_post_field(client, jsonStr, strlen(jsonStr));
    esp_err_t err = esp_http_client_perform(client);

    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "Failed to post to slack : Status = %d, content_length = %d",
                 esp_http_client_get_status_code(client),
                 esp_http_client_get_content_length(client));
    }
    esp_http_client_cleanup(client);
    free(jsonStr);

    return ESP_OK;
}

esp_err_t SLACKBOT_RegisterCmd(const char *cmd, esp_err_t (*cmdHandler)(httpd_req_t *r))
{
    return ESP_OK;
}
