# ESP32 SlackBot component for ESP-IDF #

Build status on latest ESP-IDF release : 

[![pipeline status](https://gitlab.com/AloyseTech/ESP32-SlackBot/badges/master/pipeline.svg)](https://gitlab.com/AloyseTech/ESP32-SlackBot/commits/master)

---
This component lets you interact with your Slack workspace via Webhook (device post to Slack) and Slash command (Slack requests to device, device reply).

---

## Example ##

ESP-IDF ready example : [examples/demo](https://github.com/AloyseTech/ESP32-SlackBot/tree/master/examples/demo).

## Usage ##

You must first create a Slack application, configured for Webhook and/or slash commands.

Configuration is done through the ESP-IDF `menuconfig` command.

### Required Parameters ###

`WebHook URL` is your slack application webhook URL.

`OAuth token` is your slack application generated authorization token.