#ifndef __SLACK_BOT_H__
#define __SLACK_BOT_H__

#include "esp_system.h"

esp_err_t SLACKBOT_Init();
esp_err_t SLACKBOT_Post(const char *text);

#endif //__SLACK_BOT_H__